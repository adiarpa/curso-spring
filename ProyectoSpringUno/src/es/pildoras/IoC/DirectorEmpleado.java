package es.pildoras.IoC;

public class DirectorEmpleado implements Empleados {

    // Creación de campo tipo CreacionInforme (Interfaz)
    private CreacionInformes informeNuevo;
    private String email;
    private String nombreEmpresa;

    // Creación del constructor que inyecta la dependencia
    public DirectorEmpleado(CreacionInformes informeNuevo) {
        this.informeNuevo = informeNuevo;
    }

    @Override
    public String getTareas() {
        return "Gestionar la plantilla de la Empresa";
    }

    @Override
    public String getInforme() {
        return "Informe creado por el Director: " + informeNuevo.getInforme();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
}
